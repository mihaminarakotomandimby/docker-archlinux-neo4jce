#!/bin/bash

export NEO4J_VERSION="4.4.0"
export PATH="/neo4j-enterprise-"${NEO4J_VERSION}"/bin:"${PATH}
##-p7474:7474 -p7687:7687
/usr/bin/supervisord -c /etc/supervisord.conf
