FROM archlinux:latest

RUN rm /etc/pacman.d/mirrorlist
ADD ./mirrorlist /etc/pacman.d/mirrorlist

RUN cat  /etc/pacman.d/mirrorlist

RUN pacman -Sy          \
	--noconfirm     \
	jdk11-openjdk   \
	wget            \
	bzip2           \
	which           \
	supervisor nano 
ADD ./neo4j.ini /etc/supervisor.d/
ARG NEO4J_VERSION="4.4.0"
ARG APOC_VERSION="4.4.0.1"
ADD ./neo4j-enterprise-${NEO4J_VERSION}-unix.tar.gz /
ADD ./apoc-${APOC_VERSION}-all.jar /neo4j-enterprise-${NEO4J_VERSION}/plugins/
ENV PATH="/neo4j-enterprise-"${NEO4J_VERSION}"/bin:"${PATH}
RUN neo4j-admin set-initial-password cjaneo4j
RUN sed -i 's/^#dbms.default_listen_address=0.0.0.0/dbms.default_listen_address=0.0.0.0/' /neo4j-enterprise-${NEO4J_VERSION}/conf/neo4j.conf
ADD ./entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh

ENTRYPOINT  ["/entrypoint.sh"]
